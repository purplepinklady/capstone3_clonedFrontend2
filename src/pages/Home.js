import {Fragment} from 'react';
import Banner from '../components/Banner'
import '../App.css';

 

export default function Home(){

	const data = {
		title: "Just Pets",
		content: "The only store that knows your pet needs!",
		destination: "/"
	}



		return (
			<Fragment>
				<div className="homeBg">
				<Banner data={data} />
				</div>
			</Fragment>
			)


}



